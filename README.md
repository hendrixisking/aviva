# Aviva - Interview tasks
![enter image description here](https://res-5.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco/v1421130186/kykmvarjhilqqv6djx0c.jpg)

**Last code update:  23/03/2018**

## Demo
Click [here](http://domainforssl.hu/interviewtask/aviva/) to see my solution.



# Work with the code

Open project folder

```shell
$ cd aviva
```
## To get started with the code you need install:

- Node.js / npm

- Gulp (npm install -g gulp-cli)

- Bower (npm install -g bower)

- Yeoman/generator-generator (npm install -g yo generator-generator)

- Compass (gem install compass)


## Versions:

Node version = 6.11.0

npm version = 3.10.8

Bower version = 1.8.2



## Run command:

npm install

bower install



Getting started
======

To get started with the code here you'll need a few things:

- [Node.js](http://nodejs.org/)/[npm](https://npmjs.org/)
- [Gulp](https://gulpjs.com/) (`npm install -g gulp-cli`)
- [Bower](http://bower.io/) (`npm install -g bower`)
- [Yeoman/generator-generator](http://yeoman.io/generators.html) (`npm install -g yo generator-generator`)
- [Compass](http://compass-style.org/) (`gem install compass`)


Now that you have the repository you'll need to `cd` into the working directory and download the project dependencies:

```shell
$ cd <<project folder>>
$ npm install
$ bower install
```

**NOTE: Depending on your system settings you may need to use `sudo` in front of any commands that don't execute correctly.**

Run the Gulp command to open up a local server and get all the benefits of using Gulp.

```shell
$ gulp serve 

```

To deploy to production simple type this command below. It creates a folder called dist thatcontains the production-ready files.

```shell
$ gulp 

```

---
@author : Robert Koteles
