/*********************
*  General functions
*********************/

var GeneralFunctions = {
    prevent_default: function (event) {
        'use strict';
        if (window.event) {
            window.event.returnValue = false;
        } else if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    }    
};

/*********************
*  Navigation
*********************/

var Navigation = {
    initialize: function () {
        var self = this;

        self.onClick();
    },
    onClick: function () { /*click events*/
        'use strict';
        var self = this;
       
        $(".toggle-nav").on("click", function(event) {
            GeneralFunctions.prevent_default(event);
            $("body").toggleClass("mobilemenu-open");
        });
        
        
    }
};

/*********************
*  Footer
*********************/

var Footer = {
    initialize: function () {
        var self = this;

        self.onClick();
    },
    onClick: function () { /*click events*/
        'use strict';
        var self = this;
       
        $(".footer-nav-header").on("click", function(event) {
            GeneralFunctions.prevent_default(event);
            $(this).parent('.footer-nav-col').toggleClass("accordion-open");
        });
        
        
    }
};


/*********************
*  SLick slider
*********************/

var SlickSlider = {
    initialize: function () {
        var self = this;

        $('.feedbacks').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true
		});

		$('.featured-content-list').slick({
			infinite: false,
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			responsive: [
			    {
			      breakpoint: 768,
			      settings: {
			      	centerMode: true,
			      	centerPadding: '10px',
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			  ]
		});
    }
};


/*********************
*  Video player
*********************/

var VideoPlayer = {
    initialize: function () {
        var self = this;

        self.initPopup();
    },
    initPopup: function () {
        $('.popup-video').magnificPopup({
		  type: 'iframe'
		  // other options
		});        
    }
};

(function () {
	'use strict';

	Navigation.initialize();
	SlickSlider.initialize();
	VideoPlayer.initialize();
	Footer.initialize();

})();
